import { createTheme } from '@mui/material/styles';

// Create a theme instance.
const theme = createTheme({
    palette: {
        primary: {
            main: '#ca3634' //--- red
        },
        secondary: {
            main: 'rgb(255, 223, 76)' //--- yellow
        }
    }
});

export default theme;
