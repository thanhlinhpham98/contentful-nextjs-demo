export const convertBreakToHtml = (text: string) => {
    return text && text.replace(/\r?\n/g, '<br />');
};
