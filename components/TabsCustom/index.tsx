import React from 'react';
import { Tab, Tabs, styled, TabScrollButton, Box } from '@mui/material';

export interface ITabProps {
    title: string | React.ReactNode;
    key: number;
    icon?: React.ReactElement;
}

export interface ITabsCustomProps {
    tabs: ITabProps[];
    value: number;
    isDisabled?: boolean;
    handleChangeTab: (_event: React.SyntheticEvent<Element, Event> | null, newValue: number) => void;
}

export const StyledTabScrollButton = styled(TabScrollButton)({
    width: 'fit-content'
});

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

const TabsCustom = (props: ITabsCustomProps) => {
    const { value, handleChangeTab, tabs, isDisabled } = props;

    return (
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs
                orientation='vertical'
                variant='scrollable'
                value={value}
                onChange={handleChangeTab}
                aria-label='Vertical tabs example'
                sx={{ borderRight: 1, borderColor: 'divider' }}
            >
                {tabs.map(tab => (
                    <Tab
                        disabled={isDisabled}
                        sx={{ textTransform: 'none' }}
                        {...a11yProps(tab.key)}
                        key={tab.key}
                        label={tab.title}
                        icon={tab.icon || undefined}
                        iconPosition='end'
                    />
                ))}
            </Tabs>
        </Box>
    );
};

export default TabsCustom;
