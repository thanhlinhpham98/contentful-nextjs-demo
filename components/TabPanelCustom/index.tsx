import { Box } from '@mui/material';

import React from 'react';

interface ITabPanelCustomProps {
    children: React.ReactNode;
    index: string | number;
    value: number;
    BoxProps?: object;
    noUnmount?: boolean;
}

function TabPanelCustom(props: ITabPanelCustomProps) {
    const { children, value, index, BoxProps, noUnmount, ...other } = props;
    return (
        <div>
            {(value === index || noUnmount) && (
                <div
                    role='tabpanel'
                    hidden={value !== index}
                    id={`scrollable-auto-tabpanel-${index}`}
                    aria-labelledby={`scrollable-auto-tab-${index}`}
                    {...other}
                >
                    <Box p={3} {...BoxProps}>
                        {children}
                    </Box>
                </div>
            )}
        </div>
    );
}

export default TabPanelCustom;
