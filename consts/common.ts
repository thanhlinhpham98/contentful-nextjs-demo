export const ACCESS_TOKEN = process.env.NEXT_PUBLIC_ACCESS_TOKEN;
export const SPACE_ID = process.env.NEXT_PUBLIC_SPACE_ID;
