import { AppBar, Container, Toolbar } from '@mui/material';
import Link from 'next/link';
import React from 'react';
import styles from './styles.module.scss';

interface ILayoutProps {
    children: React.ReactNode;
}

let logoUrl =
    'https://images.ctfassets.net/0q65c8wzeotw/6DI8PGrmo4rqjZYTDJyIOW/445c23f1ffaa1cf817b56f3704c0cfe2/carglass-logo.svg';

const Layout = (props: ILayoutProps) => {
    const { children } = props;
    return (
        <div className={styles.rootContainer}>
            <AppBar className={styles.appBar} color='secondary'>
                <Toolbar>
                    <Link
                        href={{
                            pathname: '/'
                        }}
                    >
                        <img src={logoUrl} alt='Carglass brand logo' height='35px' width='130px' />
                    </Link>
                </Toolbar>
            </AppBar>
            <Container className={styles.container}>{children}</Container>
        </div>
    );
};

export default Layout;
