import React from 'react';
import Link from 'next/link';
import styles from '../styles/home.module.scss';

//--- MUI
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { ACCESS_TOKEN, SPACE_ID } from '../consts/common';

const contentful = require('contentful');

const client = contentful.createClient({
    space: SPACE_ID,
    accessToken: ACCESS_TOKEN
});

export async function getStaticProps() {
    try {
        let response = await client.getEntries({ content_type: 'our_workshops', locale: 'en-US' });
        let data = response.items;
        data = data.map((d: any) => ({ ...d.fields, id: d.sys.id }));
        data = data.map((d: any) => {
            return {
                ...d,
                imageUrl: d?.image?.fields?.file?.url ? 'https:' + d.image.fields.file.url : '',
                subDescription: d?.introduce ? d.introduce.split('.')[0] : ''
            };
        });
        return {
            props: {
                data,
                response
            }
        };
    } catch (error) {
        return {
            props: {
                data: [],
                response: null
            }
        };
    }
}

export default function Home(props: { data: any[]; response: any }) {
    const { data, response } = props;
    console.log('data', data);
    console.log('response', response);

    // const getData = async (props?: { title: string }) => {
    //     let response = await client.getEntries({
    //         content_type: "our_workshops",
    //         "fields.isActive": true,
    //     });

    //     console.log("response", response);
    // };

    // useEffect(() => {
    //     getData();
    // }, []);

    return (
        <div className={styles.rootContainer}>
            {data.length && (
                <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                    {data.map((d: any, dIndex: number) => {
                        return (
                            <React.Fragment key={dIndex}>
                                <Link
                                    href={{
                                        pathname: '/[id]',
                                        query: { id: d.id }
                                    }}
                                >
                                    <ListItem alignItems='flex-start'>
                                        <ListItemAvatar>
                                            <Avatar alt={d.pageTitle} src={d.imageUrl} />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={d.pageTitle}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        sx={{ display: 'inline' }}
                                                        component='span'
                                                        variant='body2'
                                                        color='text.primary'
                                                    >
                                                        {d.branch}
                                                    </Typography>
                                                    {` — ${d.subDescription}…`}
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                </Link>
                                <Divider variant='inset' component='li' />
                            </React.Fragment>
                        );
                    })}
                </List>
            )}
        </div>
    );
}
