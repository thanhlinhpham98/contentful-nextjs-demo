import React from 'react';
import Image from 'next/image';
import { GetServerSideProps } from 'next';
import styles from '../styles/detail-page.module.scss';

//--- MUI
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';

import { ACCESS_TOKEN, SPACE_ID } from '../consts/common';
import { convertBreakToHtml } from '../utils';

const contentful = require('contentful');

const client = contentful.createClient({
    space: SPACE_ID,
    accessToken: ACCESS_TOKEN
});

export const getServerSideProps: GetServerSideProps = async context => {
    try {
        const queryId = context.query.id;
        let response = await client.getEntry(queryId);
        let data = response.fields;
        data = {
            ...data,
            imageUrl: data?.image?.fields?.file?.url ? 'https:' + data.image.fields.file.url : '',
            imageHeight: data?.image?.fields?.file?.details?.image?.height
                ? data.image.fields.file.details.image.height
                : 500,
            imageWidth: data?.image?.fields?.file?.details?.image?.width
                ? data.image.fields.file.details.image.width
                : 500
        };
        return {
            props: {
                data: data
            }
        };
    } catch (error) {
        return {
            notFound: true
        };
    }
};

const DetailPage = (props: { data: any }) => {
    const { data } = props;
    console.log('data', data);
    let languageListString = data?.language?.length ? data.language.join(', ') : '';

    return (
        <div className={styles.rootContainer}>
            <Typography component='h1' fontWeight={700} fontSize='1.5rem' lineHeight={1.17} mb='2rem'>
                {data.pageTitle}
            </Typography>

            <Grid container spacing={3} marginBottom='3rem'>
                <Grid item xs={6}>
                    <div
                        className={styles.introduce}
                        dangerouslySetInnerHTML={{
                            __html: convertBreakToHtml(data.introduce)
                        }}
                    ></div>

                    <div className={styles.linkToEmail}>
                        <a href='mailto:info@carglass.se'>
                            <ArrowRightIcon />
                            Kontakta oss via epost
                        </a>
                    </div>
                </Grid>

                <Grid item xs={6}>
                    <Button
                        color='primary'
                        variant='contained'
                        className={styles.button}
                    >{`Boka tid hos ${data.branch}`}</Button>

                    <div className={styles.content}>
                        <div className={styles.title}>{data.directionTitle}:</div>
                        <div
                            className={styles.description}
                            dangerouslySetInnerHTML={{
                                __html: convertBreakToHtml(data.direction)
                            }}
                        ></div>
                    </div>

                    <div className={styles.content}>
                        <div className={styles.title}>{data.languageTitle}:</div>
                        <div className={styles.description}>{languageListString}</div>
                    </div>
                </Grid>
            </Grid>
            <Divider />

            {/* Mid  */}
            <Typography component='h2' fontWeight={500} fontSize='1.25rem' lineHeight={1.17} mt='1.5rem'>
                {data.midTitle}
            </Typography>
            <Grid container spacing={3} marginBottom='3rem'>
                <Grid item xs={6}>
                    <div className={styles.content}>
                        <div className={styles.title}>{data.midLeftTitle}:</div>
                        <div
                            className={styles.description}
                            dangerouslySetInnerHTML={{
                                __html: convertBreakToHtml(data.midLeftDescription)
                            }}
                        ></div>
                    </div>
                </Grid>

                <Grid item xs={6}>
                    <div className={styles.content}>
                        <div className={styles.title}>{data.midLeftTitle}:</div>
                        <div
                            className={styles.description}
                            dangerouslySetInnerHTML={{
                                __html: convertBreakToHtml(data.midRightDescription)
                            }}
                        ></div>
                    </div>
                </Grid>
            </Grid>
            <Divider />

            {/* Bottom  */}
            <Typography component='h2' fontWeight={500} fontSize='1.25rem' lineHeight={1.17} mt='1.5rem'>
                {data.bottomTitle}
            </Typography>
            <Grid container spacing={3} marginBottom='3rem'>
                <Grid item xs={6}>
                    <div className={styles.content}>
                        <div className={styles.title}>{data.bottomLeftTitle}:</div>
                        <div
                            className={styles.description}
                            dangerouslySetInnerHTML={{
                                __html: convertBreakToHtml(data.bottomLeftDescription)
                            }}
                        ></div>
                    </div>
                </Grid>

                <Grid item xs={6}>
                    <div className={styles.content}>
                        <div className={styles.title}>{data.bottomRightTitle}:</div>
                        <div
                            className={styles.description}
                            dangerouslySetInnerHTML={{
                                __html: convertBreakToHtml(data.bottomRightDescription)
                            }}
                        ></div>
                    </div>
                </Grid>
            </Grid>
            <Divider />
            <Image
                className={styles.image}
                src={data.imageUrl}
                alt={data.branch}
                width={data.imageWidth}
                height={data.imageHeight}
            />
        </div>
    );
};

export default DetailPage;
